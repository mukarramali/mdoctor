class User < ActiveRecord::Base
	include BCrypt
	has_secure_password

	def self.find_or_create_from_auth_hash(auth)
		user = where(email: auth.info.email).first
		return user if user
		User.new.tap do |user|
			user.provider = auth.provider
			user.uid = auth.uid
			user.first_name = auth.info.first_name
			user.last_name = auth.info.last_name
			user.email = auth.info.email
			user.password = auth.uid
			user.save!
		end
	end
end