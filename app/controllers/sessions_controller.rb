class SessionsController < ApplicationController
  
	def create
		if social_auth_login
			@user = User.find_or_create_from_auth_hash(env["omniauth.auth"])
		elsif params[:session]
			@user = User.find_by(email: params[:session][:email].downcase)
			@user = nil unless @user && @user.authenticate(params[:session][:password])
		end

		unless @user
			redirect_to root_path
			return
		end
		session[:user_id] = @user.id
		return if password_needed?
		redirect_to :dashboard
	end

	def destroy
		session[:user_id] = nil
		redirect_to root_path
	end

	private

		def social_auth_login
			env["omniauth.auth"]
		end

		def password_needed?
			if @user.authenticate @user.uid
				redirect_to new_password_path
				return true
			end
		end
end