class DashboardController < ApplicationController

  def show
  	redirect_to :home unless user_signed_in?
	end
end
