class PasswordController < ApplicationController
	def create
		@user = current_user
		@user.password = params[:user][:password]
		if @user.save
			redirect_to :dashboard 
			return
		end
		redirect_to root_path
	end

	def new
		@user = current_user
	end
end
