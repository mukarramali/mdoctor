Rails.application.routes.draw do
  
  post 'login', to: 'sessions#create', as: 'login'
  get 'logout', to: 'sessions#destroy', as: 'logout'
  get 'auth/:provider/callback', to: 'sessions#create'
  get 'auth/failure', to: redirect('/')
  get 'home', to: 'home#show'
  get 'dashboard', to: 'dashboard#show'
  patch 'create_password', to: 'password#create'
  get 'new_password', to: 'password#new'

  root to: "home#show"
end
